﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{

    public interface ICustomerHubClient
    {

        Task ReceiveCustomerList(List<CustomerShortResponse> list);
        Task ReceiveCustomer(string error, CustomerResponse customer);
        Task ReceiveCustomerId(Guid id);
        Task ReceiveCustomerUpdateResult(string error);
        Task ReceiveCustomerDeletionResult(string error);


    }
    public class CustomersHub: Hub<ICustomerHubClient>
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        public CustomersHub(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository) {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }


        public async Task GetCustomerList()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            await Clients.Caller.ReceiveCustomerList(response);
        }

        public async Task GetCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if(customer == null)
            {
                await Clients.Caller.ReceiveCustomer("No customer with this id", null);
                return;
            }

            var response = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.Preferences.Select(x => new PreferenceResponse()
                {
                    Id = x.PreferenceId,
                    Name = x.Preference.Name
                }).ToList()

            };

            await Clients.Caller.ReceiveCustomer(null, response);
        }
        public async Task CreateCustomer(CreateOrEditCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = request.PreferenceIds == null 
                              ? new List<Preference>() 
                              : await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);
            await Clients.Caller.ReceiveCustomerId(customer.Id);

        }

        public async Task UpdateCustomer(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                await Clients.Caller.ReceiveCustomerUpdateResult("No customer with such an id.");
                return;
            }
            var preferences = request.PreferenceIds == null
                              ? new List<Preference>()
                              : await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            await Clients.Caller.ReceiveCustomerUpdateResult(null);
        }

        public async Task DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                await Clients.Caller.ReceiveCustomerDeletionResult("No customer with such an id.");
                return;
            }

            await _customerRepository.DeleteAsync(customer);

            await Clients.Caller.ReceiveCustomerDeletionResult(null);
        }

    }
}
