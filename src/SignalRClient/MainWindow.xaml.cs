﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SignalRClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HubConnection _connection;
        private bool _connected = false;
        private bool _closed = false;

        public MainWindow()
        {
            InitializeComponent();
            _connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/CustomersHub")
                .WithAutomaticReconnect(new[] { TimeSpan.Zero, TimeSpan.Zero, TimeSpan.FromSeconds(10) })
                .Build();

            _connection.On<List<CustomerShortResponse>>("ReceiveCustomerList", list =>
            {
                Dispatcher.BeginInvoke(() => {
                    OutputTextBox.Text = CustomerListToString(list); 
                });
            });
            _connection.On<string, CustomerResponse>("ReceiveCustomer", (error, customer) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    OutputTextBox.Text = string.IsNullOrEmpty(error) ? CustomerToString(customer) : error;
                });
            });
            _connection.On<Guid>("ReceiveCustomerId", id => { 
                Dispatcher.BeginInvoke(() =>
                {
                    OutputTextBox.Text = id.ToString();
                });
            });
            _connection.On<string>("ReceiveCustomerUpdateResult", error =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    OutputTextBox.Text = String.IsNullOrEmpty(error) ? "Success." : error;
                });

            });
            _connection.On<string>("ReceiveCustomerDeletionResult", error =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    OutputTextBox.Text = String.IsNullOrEmpty(error) ? "Success." : error;
                });

            });

        }

        private string CustomerToString(CustomerResponse customer)
        {
            string result = $"Id: {customer.Id}\nName: {customer.FirstName} {customer.LastName}\nEmail: {customer.Email}\n";
            if (customer.Preferences == null)
                result += "Preferences: null\n";
            else
            {
                result += "Preferences: [";
                bool isFirstItem = true;
                foreach (var pref in customer.Preferences)
                {
                    if (!isFirstItem)
                    {
                        result += ", ";
                    }
                    result += pref.Name;
                    isFirstItem = false;
                }
                result += "]\n";
            }
            result += "Promocodes: ";
            if (customer.PromoCodes != null) { 
                result += "[\n";
                foreach (var pc in customer.PromoCodes)
                {
                    result += $"Code: {pc.Code}; Partner: {pc.PartnerName} ServiceInfo: {pc.ServiceInfo}; StartDate: {pc.BeginDate}; EndDate: {pc.EndDate}\n";
                }
                result += "]";
            }
            else
            {
                result += " null";
            }
            return result ;
        }

        private string CustomerListToString(List<CustomerShortResponse> list)
        {
            string result = "";

            bool isFirstItem = true;
            foreach (var item in list)
            {
               if (!isFirstItem) result += ",\n";
               result += $"Id: {item.Id}; Name: {item.FirstName} {item.LastName}; Email: {item.Email}";
               isFirstItem= false;
            }

            return result;
        }

        private async Task<bool> EnsureConnection()
        {
            
            if (_connection.State == HubConnectionState.Connected) return true;
            if(_connection.State == HubConnectionState.Disconnected)
            {
                await _connection.StartAsync();
                return _connection.State == HubConnectionState.Connected;
            }
             return false;
        }

        private async Task SafeInvokeMethod(Func<Task> call)
        {
            if (!await EnsureConnection()) return;
            try
            {
                await call();
            }
            catch (HubException e)
            {
                MessageBox.Show(e.Message);
            }
        }


        private async void CustomerListButton_Click(object sender, RoutedEventArgs e)
        {
            await SafeInvokeMethod(() => _connection.InvokeAsync("GetCustomerList"));
        }

        private async void CustomerButton_Click(object sender, RoutedEventArgs args)
        {
            await SafeInvokeMethod(() => _connection.InvokeAsync("GetCustomer", Guid.Parse(CustomerID_TextBox.Text)));
        }

        private async void CreateCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            string prefIds = CustomerPreferenceIds_TextBox.Text;
            var parameters = new CreateOrEditCustomerRequest()
            {
                FirstName = CustomerName_TextBox.Text,
                LastName = CustomerLastName_TextBox.Text,
                Email = CustomerEmail_TextBox.Text,
                PreferenceIds = (String.IsNullOrEmpty(prefIds)? null : new List<Guid>(prefIds.Split(';').Select(line => Guid.Parse(line))))!
            };
            await SafeInvokeMethod(() => {
                return _connection.InvokeAsync("CreateCustomer", parameters);
            });
        }

        private async void UpdateCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            Guid id = Guid.Parse(CustomerID2_TextBox.Text);
            string prefIds = CustomerPreferenceIds2_TextBox.Text;
            var parameters = new CreateOrEditCustomerRequest()
            {
                FirstName = CustomerName2_TextBox.Text,
                LastName = CustomerLastName2_TextBox.Text,
                Email = CustomerEmail2_TextBox.Text,
                PreferenceIds = (String.IsNullOrEmpty(prefIds) ? null : new List<Guid>(prefIds.Split(';').Select(line => Guid.Parse(line))))!
            };
            await SafeInvokeMethod(() => {
                return _connection.InvokeAsync("UpdateCustomer", id, parameters);
            });

        }

        private async void DeleteCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            Guid id = Guid.Parse(CustomerID3_TextBox.Text);
            await SafeInvokeMethod(() => {
                return _connection.InvokeAsync("DeleteCustomer", id);
            });
        }
    }
}
